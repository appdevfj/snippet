#Owner: Calev Chandra
#Date: 2018/09/20

App: Sylo Snippet

The project has been created in React JS and the following instructions will be a  guide for running and verifying application on the browser.

Steps to run:
1. Unzip file and go to client folder in terminal 
"...\ipfs-snippet_loader\client"

2. Given node or yarn is already installed in your computer, run following command in terminal
"yarn start"
or
"npm start"

3. Should see the snippet home page render on browser with similar urL: "http://localhost:3000/"

Features of snippet:
1. Add new paste
2. Edit current paste
3. Create snippet with versions with a view
4. Ability to download the latest snippet version in xml format.

Note: Data is retrieved from IPFS and not stored on the database. Hence, loading project page would reset initial request data 
and have to start over again.

Technology used:
- React
- Javascript6
- IPFS API
- JSON 
- XMLHttpRequest - I had difficulty in integrating react-saga at this time and used the traditional approach. Will be implementing react-saga in the next 
release should I be permitted.

Next steps:
- Integrate Truffle and Drizzle with smart contract to enable data storage to the block chain
- React-Saga replacing XMLHttpRequest method
- Refactor code

End