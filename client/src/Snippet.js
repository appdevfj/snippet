export default class Snippet {
  constructor() {
    name: "";
    content: "";
  }

  fetchSnippet(name, content) {
  	let currentdate = Math.floor(Date.now() / 1000);

  	let jsonData = { 
		  "name"  :  name, 
		  "content"   :  content,
		  "modifiedDate" : currentdate
	}
    return jsonData;
    
  }
}