//connecting to a remote node instead of connecting to a local node
const IPFS = require("ipfs-api");
const ipfs = new IPFS({
  host: "ipfs.infura.io",
  port: 5001,
  protocol: "https"
});

export default ipfs;
