import React, { Component } from "react";
import Snippet, { fetchSnippet } from "./Snippet";
import EngineFunction from "./EngineFunction";
import "./App.css";
import ipfs from "./ipfs";

import "./css/oswald.css";
import "./css/flatly.css";
import "./css/open-sans.css";
import "./css/pure-min.css";
import "./css/snippet.css";


class App extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();

    this.state = {
      ipfsHash: '',
      web3: null,
      buffer: null,
      snippetContent: null,
      outputData: null,
      renderData: [],
      pasteCodeData: "",
      pasteName: "",
      button_status : true,
      filename_breadcrumb: true,
      file_link_url: ""
    };
    this.captureFile = this.captureFile.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.clearData = this.clearData.bind(this);
    this.onViewSnippet = this.onViewSnippet.bind(this);
    this.getFile = this.getFile.bind(this);
  }

  // Snippet text area and file name validator.
  captureFile(event) {
    const snippetData = new Snippet();
    const engine = new EngineFunction();
    console.log("capture file");
    event.preventDefault();

    this.setState({pasteCodeData: document.getElementById("paste_code").value});
    this.setState({pasteName: document.getElementById("paste_name").value});

    (this.state.pasteName.length > 0 && this.state.pasteCodeData.length > 0) ? 
    this.setState({button_status : false}) : this.setState({button_status : true});

  }

  // Create button click ; creates snippet and adds to ipfs
  onSubmit(event) {
    const snippetData = new Snippet();
    const engine = new EngineFunction();

    event.preventDefault();

    let cached_data = "",snip_header = null;
    const finalData = snippetData.fetchSnippet(this.state.pasteName,this.state.pasteCodeData);

    (this.state.outputData === null) ? cached_data = JSON.stringify(finalData) : cached_data =this.state.outputData+","+ JSON.stringify(finalData);

    this.setState({ outputData: cached_data}, () => {
    });

    let x = "", i =0, jsonSnippetData = "["+ cached_data +"]", filteredbyName = "";
    if(cached_data != "" && cached_data != null){
      jsonSnippetData = JSON.parse(jsonSnippetData);

      //filer by name then render
      filteredbyName = jsonSnippetData.filter(l => {
        return l.name.toLowerCase().match( this.state.pasteName.toLowerCase() );
      });
      // Sort by version
      filteredbyName.sort(engine.sort_by('modifiedDate', true, parseInt));

      this.setState({renderData: filteredbyName})
    }

    const strBuffer =  engine.textAreaToArrayBuffer(JSON.stringify(finalData));

    //Adding file to IPFS
    ipfs.files.add(Buffer(strBuffer), (error, result) => {
      if (error) {
        console.error(error);
        return;
      }
       this.setState({ ipfsHash: result[0].hash });

       let url = "https://ipfs.io/ipfs/".concat( this.state.ipfsHash ); 
       console.log("url", url);
       this.setState({file_link_url: url});
       engine.xmlHTTPRequestAPICall(url,"pastehere");
    });

    this.setState({filename_breadcrumb: false})
    this.setState({button_status : true});
    this.clearData();

    snip_header = document.getElementById("snippet_header");
    snip_header.scrollIntoView();
  }  
  // Clears data in text area and name field
  clearData(){
    this.setState({pasteCodeData : ""});
    document.getElementById("paste_code").value = "";
    document.getElementById("paste_name").value = "";
  }

  //Edit button on click method
  onViewSnippet(pasteContent,pasteName){
    let edit_section = document.getElementById("edit_section");
    edit_section.scrollIntoView({ behavior: 'smooth' });

    document.getElementById("paste_code").value = pasteContent;
    document.getElementById("paste_name").value = pasteName;
  }

  // Open file from IPFS
  getFile(){
    window.open(this.state.file_link_url);
  }

  render() {
    const {renderData} = this.state;
    let counter = 0;

    return (
      <div className="App">
        <nav className="navbar pure-menu pure-menu-horizontal">
          <a href="#" className="pure-menu-heading pure-menu-link">
            Sylo Paste Bin
          </a>
        </nav>

        <main className="container">
          <div  className="pure-g">
            <div className="pure-u-1-1">
              <div id="snippet_header">
              <h1 >Your Snippet</h1>
              <ol hidden={this.state.filename_breadcrumb} class="breadcrumb">
                <li class="breadcrumb-item active">Filename: {this.state.pasteName}.xml</li>
              </ol>
              </div>
             <table  class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Version</th>
                    <th scope="col">Content</th>
                    <th scope="col">Action</th>
                    <th scope="col">File</th>
                  </tr>
                </thead>
                <tbody id="table_data">
                {
                  renderData.length > 0 ? renderData.map(snippet_data => {
                    counter ++;
                    const {name,content,modifiedDate} = snippet_data;

                    if(counter > 1){
                      return <tr key={modifiedDate} >
                          <th scope="row">{modifiedDate}</th>
                          <td id="current_content">{content}</td>
                          </tr>
                    } else {
                      return <tr key={modifiedDate} class="table-active">
                          <th scope="row">{modifiedDate}</th>
                          <td>{content}</td>
                          <td><button type="button" class="btn btn-success"
                          onClick={()=>this.onViewSnippet(content,name)} 
                          >Edit</button></td>
                          <td><button type="button" class="btn btn-link"
                          onClick={()=>this.getFile()} 
                          >{this.state.file_link_url}</button></td>
                          </tr>
                    }  
                  }): null
                }
                </tbody>
              </table>  
              <h2>Paste</h2>
              <div id="edit_section" class="form-group">
                <textarea  class="form-control" placeholder="" id="paste_code" rows="5" placeholder="Add here"  onChange={this.captureFile}
                 ></textarea>
              </div>

              <div class="form-group">
                <label for="snipprtInputFilename">Name/Title:</label>
                <input type="text" class="form-control" id="paste_name"  placeholder="Enter name" onChange={this.captureFile}/>
                
              </div>

              <div className="form_right">        
              <form onSubmit={this.onSubmit}>
                <input disabled = {this.state.button_status} type="submit" value="Create" class="btn btn-primary btn-lg"/>
              </form>
              </div>              
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App;