
export default class EngineFunction {
	//Converts data from TextArea field to Buffer to be added to IPFS
	textAreaToArrayBuffer= (str) => {
	   let buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
	   let bufView = new Uint16Array(buf);
	   for (let i = 0, strLen = str.length; i < strLen; i++) {
	   bufView[i] = str.charCodeAt(i);
	  }
	  return buf;
	}

	//retrieve data  from the ipfs API using XMLHttpRequest (to incorporate redux-saga here)
	xmlHTTPRequestAPICall =(url,textAreaID) =>{
    let xhttp = new XMLHttpRequest(),allText = ""; 
    xhttp.open("GET", url, true);
    
    xhttp.overrideMimeType('text/xml');
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState === 4) {  // Makes sure the document is ready to parse.
        if (xhttp.status === 200) {  // Makes sure it's found the file.
          allText = String.fromCharCode.apply(null, new Uint16Array(xhttp.response));
        }
      }
    }
    xhttp.responseType = "arraybuffer";
    xhttp.send(null);     
  }

  //Sort data by version
  sort_by = (field, reverse, primer) =>{

   var key = primer ? 
       function(x) {return primer(x[field])} : 
       function(x) {return x[field]};

     reverse = !reverse ? 1 : -1;

     return function (a, b) {
         return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
       } 
  }
}
